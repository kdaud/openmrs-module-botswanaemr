package org.openmrs.module.botswanaemr.api.dao;

import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryPerson;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface BotswanaPersonRegistryDao {
	List<BotswanaRegistryPerson> getPersonsById(String personId) throws SQLException, ParseException;
}
