/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 * <p>
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.dao.BotswanaEmrDao;

import java.util.Date;
import java.util.List;

/**
 * Implementations of business logic methods for botswanaEMR
 */
public class BotswanaEmrServiceImpl extends BaseOpenmrsService implements BotswanaEmrService {

  protected static final Log log = LogFactory.getLog(BotswanaEmrServiceImpl.class);

  public BotswanaEmrDao getDao() {
    return dao;
  }

  public void setDao(BotswanaEmrDao dao) {
    this.dao = dao;
  }

  private BotswanaEmrDao dao;


  @Override
  public List<Patient> getPatientsRegisteredOnDate(Date startDate, Date endDate, Integer limit) {
    return dao.getPatientsCountRegisteredOnDate(startDate, endDate, limit);
  }

  @Override
  public List<Patient> getPatientsSeenOnDatePerHour(Date date, String hour) {
    return dao.getPatientsSeenOnDatePerHour(date, hour);
  }
}
