package org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public @Data
class BotswanaRegistryPerson {
	private String idNumber;
	private String fullName;
	private String gender;
	private Date dateOfBirth;
	@Setter(AccessLevel.NONE) private float age;

	public void setAge(float age) {
		this.age = age;
	}
}
