package org.openmrs.module.botswanaemr.api;

import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;

import java.sql.SQLException;
import java.text.ParseException;

public interface BotswanaPersonRegistryService {

	public BotswanaRegistryResponse searchPerson(String personId);

}
