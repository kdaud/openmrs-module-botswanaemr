package org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry;

import lombok.Data;

public @Data
class BotswanaRegistryResponse {
	String status;
	Object response;

}
