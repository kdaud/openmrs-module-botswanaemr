package org.openmrs.module.botswanaemr;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.Person;
import org.openmrs.PersonName;
import org.openmrs.api.context.Context;
import org.openmrs.util.OpenmrsUtil;

import java.lang.reflect.Field;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class BotswanaemrUtils {

  public static String formatDateWithTime(Date date) {

    Format formatter = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

    return formatter.format(date);
  }

  /**
   * Calculates the days since the given date
   *
   * @param date1 the date
   * @param date2 the date2
   * @return the number of days
   */
  public static int unitsSince(Date date1, Date date2, String type) {
    int value = 0;
    DateTime d1 = new DateTime(date1.getTime());
    DateTime d2 = new DateTime(date2.getTime());
    if(type.equals("days")){
      value = Math.abs(Days.daysBetween(d1, d2).getDays());
    }
    else if(type.equals("hours")){
      value = Math.abs(Hours.hoursBetween(d1, d2).getHours());
    }
    else if(type.equals("minutes")){
      value = Math.abs(Minutes.minutesBetween(d1, d2).getMinutes());
    }
    else if(type.equals("seconds")){
      value = Math.abs(Seconds.secondsBetween(d1, d2).getSeconds());
    }
    else if(type.equals("years")){
      value = Math.abs(Years.yearsBetween(d1, d2).getYears());
    }
    else if(type.equals("weeks")){
      value = Math.abs(Weeks.weeksBetween(d1, d2).getWeeks());
    }
    else if(type.equals("months")){
      value = Math.abs(Months.monthsBetween(d1, d2).getMonths());
    }
    return value;
  }

  /**
   * Formats a person name
   * @param name the name
   * @return the string value
   * @should format voided person as empty string
   */
  public static String formatPersonName(PersonName name) {
    if (name != null) {
      List<String> items = new ArrayList<String>();

      if (name.getFamilyName() != null) {
        items.add(name.getFamilyName() + ",");
      }
      if (name.getGivenName() != null) {
        items.add(name.getGivenName());
      }
      if (name.getMiddleName() != null) {
        items.add(name.getMiddleName());
      }
      return OpenmrsUtil.join(items, " ");
    }
    return "";
  }

  /**
   * Formats a person creator
   * @param person the name
   * @return the string value
   * @should format voided person as empty string
   */
  public static String formatPersonCreator(Person person) {
    if (person != null) {
      List<String> items = new ArrayList<String>();

      if (person.getCreator() != null) {
        if(person.getCreator().equals(Context.getAuthenticatedUser())) {
          items.add("You");
        }
        else {
          if (person.getCreator().getFamilyName() != null) {
            items.add(person.getCreator().getFamilyName() + ",");
          }
          if (person.getCreator().getGivenName() != null) {
            items.add(person.getCreator().getGivenName());
          }
        }
      }
      return OpenmrsUtil.join(items, " ");
    }
    return "";
  }

  public static String formatDurationSinceRegistration(Person person) {

    String duration = "";
    if(person != null) {
      if(unitsSince(new Date(), person.getDateCreated(), "seconds") > 0 && unitsSince(new Date(), person.getDateCreated(), "seconds") < 60){
        duration = unitsSince(new Date(), person.getDateCreated(), "seconds")+" seconds ago";
      }
      else if(unitsSince(new Date(), person.getDateCreated(), "minutes") > 0 && unitsSince(new Date(), person.getDateCreated(), "minutes") < 60){
        duration = unitsSince(new Date(), person.getDateCreated(), "minutes")+" minutes ago";
      }
      else if(unitsSince(new Date(), person.getDateCreated(), "hours") > 0 && unitsSince(new Date(), person.getDateCreated(), "hours") < 24){
        duration = unitsSince(new Date(), person.getDateCreated(), "hours")+" hours ago";
      }
      else if(unitsSince(new Date(), person.getDateCreated(), "days") > 0 && unitsSince(new Date(), person.getDateCreated(), "days") < 7){
        duration = unitsSince(new Date(), person.getDateCreated(), "days")+" days ago";
      }
      else if(unitsSince(new Date(), person.getDateCreated(), "weeks") > 0 && unitsSince(new Date(), person.getDateCreated(), "weeks") < 4){
        duration = unitsSince(new Date(), person.getDateCreated(), "weeks")+" weeks ago";
      }
      else if(unitsSince(new Date(), person.getDateCreated(), "months") > 0 && unitsSince(new Date(), person.getDateCreated(), "months") < 12){
        duration = unitsSince(new Date(), person.getDateCreated(), "months")+" months ago";
      }
      else if(unitsSince(new Date(), person.getDateCreated(), "years") > 0){
        duration = unitsSince(new Date(), person.getDateCreated(), "years")+" years ago";
      }

    }

    return duration;
  }

  public static String formatGender(Person person) {

    String gender = "";
    if(person != null) {
      if(person.getGender().equals("M")) {
        gender = "Male";
      }
      else if(person.getGender().equals("F")) {
        gender = "Female";
      }
    }
    return gender;
  }

  public static String formatPatientIdentifier(Patient patient) {
    String identifier = "";
    if(patient != null && patient.getIdentifiers() != null) {
        for(PatientIdentifier patientIdentifier:patient.getIdentifiers()) {
          identifier = patientIdentifier.getIdentifier();
        }
    }
    return identifier;
  }

  public static String readAttributeValue(Map<String, String> obj, String fieldName){
    return obj.get(fieldName);

  }

  public static String extractFamilyName(String fullname) {
    String[] splitName = fullname.split(" ");
    if (splitName.length > 0 ) {
      return splitName[0];
    }
    return "";
  }

  public static String extractGivenName(String fullname) {
    String[] splitName = fullname.split(" ");
    if (splitName.length > 1 ) {
      return splitName[1];
    }
    return "";
  }

  public static String extractMiddleName(String fullname) {
    String[] splitName = fullname.split(" ");
    if (splitName.length > 2 ) {
      return splitName[2];
    }
    return "";
  }


}
