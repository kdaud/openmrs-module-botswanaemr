/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openmrs.Patient;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.module.botswanaemr.api.dao.BotswanaEmrDao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HibernateBotswanaEMRDao implements BotswanaEmrDao {

  SimpleDateFormat formatterExt = new SimpleDateFormat("yyyy-MM-dd");
  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  public DbSessionFactory getDbSessionFactory() {
    return dbSessionFactory;
  }

  public void setDbSessionFactory(DbSessionFactory dbSessionFactory) {
    this.dbSessionFactory = dbSessionFactory;
  }

  private DbSessionFactory dbSessionFactory;


  public List<Patient> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit) {

    Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Patient.class);

    String startDateStr = "";
    String endDateStr = "";
    String startFromDate = "";
    String endFromDate = "";
    if(startDate !=null && endDate != null) {
      startDateStr = formatterExt.format(startDate);
      endDateStr = formatterExt.format(endDate);
      startFromDate = startDateStr + " 00:00:00";
      endFromDate = endDateStr + " 23:59:59";
    }
    criteria.add(Restrictions.eq("voided", false));

      try {
        criteria.add(Restrictions.and(Restrictions.ge(
                "dateCreated", formatter.parse(startFromDate)), Restrictions.le(
                "dateCreated", formatter.parse(endFromDate))));
      } catch (Exception e) {
        // TODO: handle exception
        System.out.println("Error convert date: " + e.toString());
        e.printStackTrace();
      }

    criteria.addOrder(Order.desc("dateCreated"));
    if(limit != null) {
      criteria.setFetchSize(limit);
    }
    return CollectionUtils.isNotEmpty(criteria.list()) ? criteria.list() : null;
  }

  @Override
  public List<Patient> getPatientsSeenOnDatePerHour(Date date, String hour) {
    Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Patient.class);

    String startDateStr = formatterExt.format(date);
    String startFromDateTime = startDateStr +" "+hour+":00:00";
    String endToDateTime = startDateStr +" "+hour+":59:59";
    try {
      criteria.add(Restrictions.and(Restrictions.ge(
              "dateCreated", formatter.parse(startFromDateTime)), Restrictions.le(
              "dateCreated", formatter.parse(endToDateTime))));
    } catch (Exception e) {
      // TODO: handle exception
      System.out.println("Error convert date: " + e.toString());
      e.printStackTrace();
    }
    return CollectionUtils.isNotEmpty(criteria.list()) ? criteria.list() : null;
  }
}
