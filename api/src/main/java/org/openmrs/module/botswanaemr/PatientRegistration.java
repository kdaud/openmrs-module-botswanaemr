package org.openmrs.module.botswanaemr;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data
class PatientRegistration implements Serializable {

	String patientType;

	String idType;

	String idNumber;

	String fullname;

	String gender;

	String dob;

	String email;

	String contactNumber;

	String occupation;

	String employerName;

	String homeaddress;

	String nokIdNumber;

	String nokFullname;

	String nokRelationship;

	String nokContact;

	String nokEmail;

}
