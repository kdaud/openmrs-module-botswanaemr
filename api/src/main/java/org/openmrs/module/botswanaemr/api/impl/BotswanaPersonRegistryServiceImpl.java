package org.openmrs.module.botswanaemr.api.impl;

import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryPerson;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistryService;
import org.openmrs.module.botswanaemr.api.dao.BotswanaPersonRegistryDao;
import org.openmrs.module.botswanaemr.api.dao.oracle.BotswanaPersonRegistryDaoImpl;
import org.springframework.http.HttpStatus;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public class BotswanaPersonRegistryServiceImpl implements BotswanaPersonRegistryService {

	private BotswanaRegistryResponse botswanaRegistryResponse;

	@Override
	public BotswanaRegistryResponse searchPerson(String personIdNumber) {

		BotswanaPersonRegistryDao botswanaPersonRegistryDao =  new BotswanaPersonRegistryDaoImpl();
		List<BotswanaRegistryPerson> personsById = null;

		try {
			botswanaRegistryResponse = new BotswanaRegistryResponse();
			personsById = botswanaPersonRegistryDao.getPersonsById(personIdNumber);

			botswanaRegistryResponse.setResponse(personsById);
			botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
		}
		catch (SQLException e) {
			botswanaRegistryResponse.setResponse(e.getMessage());
			botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
		}
		catch (ParseException e) {
			botswanaRegistryResponse.setResponse(e.getMessage());
			botswanaRegistryResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		}

		return botswanaRegistryResponse;
	}
}
