jq = jQuery;

jq(function () {
    function retrievePatientId() {
        const params = new URLSearchParams(window.location.search);
        return params.get("patientId");
    }

    jq("#amount").on("keyup", function () {
        if (jq(this).val() !== "" && jq("input[name='paymentRadio']").is(":checked") === true) {
            jq('#btnCapturePayment').prop('disabled', false);
        } else {
            jq('#btnCapturePayment').prop('disabled', true);
        }
    });

    jq("input[name='paymentRadio']").on("change", function () {
        if (jq(this).val() !== "" && jq("#amount").val() !== "" && jq("input[name='paymentRadio']").is(":checked") === true) {
            jq('#btnCapturePayment').prop('disabled', false);
        } else {
            jq('#btnCapturePayment').prop('disabled', true);
        }
    });

    jq("#assignPatient").change(function () {
        jq('#btnAssignPatient').prop('disabled', false);
    });

    jq('#capturePayment').submit(function (e) {
        e.preventDefault();
        let patientId = retrievePatientId();
        window.location.href = "assignPatient.page?patientId=" + patientId;

    });
});