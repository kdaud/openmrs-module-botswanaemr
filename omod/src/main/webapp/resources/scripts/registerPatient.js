jq = jQuery;

jq(function () {
    $("#patientIdError").hide();
    $("#patientSpinner").hide();
    $("#nokIdError").hide();
    $("#nokSpinner").hide();

    const SEARCH_REGISTRY_TEXT = "Searching registry..."
    const ID_NOT_FOUND = "ID not found in registry. Register patient manually.";
    const NOK_ID_NOT_FOUND = "ID not found in registry. Register next of kin manually.";

    function toSentenceCase(inputString) {
        let result = inputString.replace(/([A-Z])/g, " $1");
        result = result.charAt(0).toUpperCase() + result.slice(1);
        return result;
    }

    // Gets a date value in numeric format and converts it to the "yyyy-MM-dd" format
    function numericToIsoDateFormat(dateValue) {
        let date = new Date(dateValue);
        let dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
                            .toISOString()
                            .split("T")[0];
        return dateString;
    }

    $('#btnPrevious').on('click', function () {
        $(".sw-btn-prev").trigger("click");
    })

    $('#btnNext').on('click', function () {
        const hash = window.location.hash;
        if (hash.endsWith('-3')) {
            // TODO : Register patient here
            var settings = {
              "url": "/" + OPENMRS_CONTEXT_PATH +"/ws/rest/v1/botswanaemr/registration",
              "method": "POST",
              "timeout": 0,
              "headers": {
                "Content-Type": "application/json",
              },
              "data": JSON.stringify({
                "patientType": "Regular",
                "idType": "NationalId",
                "idNumber": $("#patientId").val(),
                "fullname": $("#name").val(),
                "gender": $("#gender").val(),
                "dob": $("#dateOfBirth").val(),
                "email": $("#email").val(),
                "contactNumber": $("#phoneNumber").val(),
                "occupation": $("#occupation").val(),
                "employerName": $("#employer").val(),
                "homeaddress": $("#homeAddress").val(),
                "nokIdNumber": $("#nokIDNumber").val(),
                "nokFullname": $("#nokFullName").val(),
                "nokRelationship": $("#nokRelationship").val(),
                "nokContact": $("#nokContact").val(),
                "nokEmail": $("#nokEmail").val()
              }),
            };

            $.ajax(settings).done(function (response) {
                if (response.status.indexOf('200') > -1) {
                    window.location.href = "capturePayment.page?patientId=" + $("#patientId").val();
                } else {
                    // Display error
                    alert("Error: " + response.response);
                }
                console.log(response);
            }).fail(function (response) {
                alert("Failed: " + response.statusText);
                console.log(response)
            });
        } else if(hash.endsWith('-1') && $("#formPatientDetails").valid()) {
                $(".sw-btn-next").trigger("click");

        } else if (hash.endsWith('-2') && $("#formNOK").valid()) {
            $(".sw-btn-next").trigger("click");
        }
    })

    $("#formPatientDetails :input").change(function () {
        let inputs = $('#formPatientDetails :input');
        let formData = {};

        inputs.each(function () {
            if ($(this).val() !== "" && $(this).val() !== null) {
                formData[this.name] = $(this).val();
            }
        });
        $('#generalDetails').empty();
        for (const val in formData) {
            $('#generalDetails').append('<p>Patient\'s ' + toSentenceCase(val) + ': ' + formData[val] + '</p>')
        }
    });

    $("#formNOK :input").change(function () {
        let inputs = $('#formNOK :input');
        let formData = {};

        inputs.each(function () {
            if ($(this).val() !== "" && $(this).val() !== null) {
                formData[this.name] = $(this).val();
            }
        });
        $('#nokDetails').empty();
        for (const val in formData) {
            $('#nokDetails').append('<p>Next of kin ' + toSentenceCase(val) + ': ' + formData[val] + '</p>')
        }
    });

    function setInputVal(id, val) {
        $(id).val(val);
        $(id).prop("disabled", true);
        $(id).valid();
    }

    function resetInputs() {
        $("#formPatientDetails :input").prop("disabled", false);
        $('#name').val("");
        $('#gender').val("");
        $('#age').val("");
        $('#dateOfBirth').val("");
    }

    function resetNokInputs() {
        $("#formNOK :input").prop("disabled", false);
        $('#nokFullName').val("");
    }

    $("#patientId").on("keyup", function () {
        if ($(this).val().length >= 9) {
            $("#patientSpinner").show();
            $("#patientIdError").show();
            $("#patientIdError").text(SEARCH_REGISTRY_TEXT);
            const url = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/registry/' + $(this).val();

            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    if (response.status.indexOf('200') > -1) {
                        $("#patientIdError").hide();
                        if (response.response.length > 0) {
                            const patientDetails = response.response[0];

                            if (patientDetails.fullName !== "") {
                                setInputVal("#name", patientDetails.fullName);
                            }
                            if (patientDetails.gender !== "") {
                                setInputVal("#gender", patientDetails.gender);
                            }
                            if (patientDetails.age !== "") {
                                setInputVal("#age", patientDetails.age);
                            }
                            if (patientDetails.dateOfBirth !== "") {
                                if ($.isNumeric(patientDetails.dateOfBirth)) {
                                    setInputVal("#dateOfBirth", numericToIsoDateFormat(patientDetails.dateOfBirth));
                                } else {
                                    setInputVal("#dateOfBirth", patientDetails.dateOfBirth);
                                }
                            }
                        }
                    } else {
                        resetInputs();
                        $("#patientIdError").text(ID_NOT_FOUND);
                        $("#patientIdError").show();
                    }

                    $("#patientSpinner").hide();
                },
                error: function (response) {
                    resetInputs();
                    $("#patientIdError").text(ID_NOT_FOUND);
                    $("#patientIdError").show();
                    $("#patientSpinner").hide();
                }
            });
        }
    });

    $("#nokIDNumber").on("keyup", function () {
        if ($(this).val().length >= 9) {
            $("#nokSpinner").show();
            $("#nokIdError").text(SEARCH_REGISTRY_TEXT);
            $("#nokIdError").show();

            const url = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/registry/' + $(this).val();

            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    if (response.status.indexOf('200') > -1) {
                        $("#nokIdError").hide();
                        if (response.response.length > 0) {
                            const nokDetails = response.response[0];

                            if (nokDetails.fullName !== "") {
                                setInputVal("#nokFullName", nokDetails.fullName);
                            }
                        }
                    } else {
                        resetNokInputs();
                        $("#nokIdError").text(NOK_ID_NOT_FOUND);
                        $("#nokIdError").show();
                    }

                    $("#nokSpinner").hide();
                },
                error: function (response) {
                    resetNokInputs();
                    $("#nokIdError").text(NOK_ID_NOT_FOUND);
                    $("#nokIdError").show();
                    $("#nokSpinner").hide();
                }
            });
        }
    });

});
