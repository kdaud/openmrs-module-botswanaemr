<%
    def afterSelectedUrl = '/botswanaemr/patientProfile.page?patientId={{ patientId }}'
%>
<script type="text/javascript">
    jq(function() {
        var resultTemplate = _.template(jq('#result-template').html());
        jq('#patient-search-form').submit(function() {
            var query = jq('#patient-search').val();
            var customRep = 'custom:(uuid,identifiers:(identifierType:(name),identifier),person)';
            jq.getJSON('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/patient', {v: customRep, q: query }, function(data) {
                var resultTarget = jq('#patient-search-results');
                resultTarget.html('');
                _.each(data.results, function(patient) {
                    var url = '/' + OPENMRS_CONTEXT_PATH + emr.applyContextModel('${ ui.escapeJs(afterSelectedUrl) }', { patientId: patient.uuid });
                    resultTarget.append(resultTemplate({ patient: patient, url: url }));
                });
            });
            return false;
        });

        jq('#patient-search').focus();

        jq('#patient-search-results').on('click', '.patient-search-result', function(evt) {
            location.href = jq(this).find('a.btn').attr('href');
        });
    });
</script>

<form method="get" id="patient-search-form">
    <div class="input-group input-group-lg">
        <input type="text" class="form-control" id="patient-search" placeholder="${ ui.message("coreapps.findPatient.search.placeholder") }">
        <div class="input-group-append">
            <button class="btn btn-md btn-primary mt-0" type="submit">
                <i class="fa fa-search"></i> 
            </button>
        </div>
    </div>
</form>

<ul id="patient-search-results"></ul>

<script type="text/template" id="result-template">
    <li class="patient-search-result list-group-item" data-patient-uuid="{{- patient.uuid }}">
        {{ _.each(patient.identifiers, function(id) { }}
            <span class="patient-identifier">
                <span class="identifier-type">{{- id.identifierType.name }}</span>
                <span class="identifier">{{- id.identifier }}</span>
            </span>
        {{ }) }}
        <span class="preferred-name">
            {{- patient.person.preferredName.display }}
        </span>
        <span class="age">
            {{- patient.person.age }}
        </span>
        <span class="gender">
            {{- patient.person.gender }}
        </span>
        <a class="btn btn-primary text-white" href="{{= url }}">${ ui.message("coreapps.findPatient.result.view") }</a>
    </li>
</script>



