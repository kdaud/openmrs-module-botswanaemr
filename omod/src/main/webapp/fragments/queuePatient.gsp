<form id="queuePatientForm" method="post">
    <div>
        <div class="form-group content-section">
            <label for="queueRoom">Assign patient to
                <span class="text-danger">*</span>
            </label>
            <select class="custom-select" id="queueRoom" name="queueRoom">
                <option disabled selected>Select where the patient needs to be assigned to</option>
                <% if (locationList != null) {
                    locationList.each { %>
                <option value="${it.uuid}">${it.name}</option>
                <%
                        }
                    }
                %>
            </select>
        </div>

        <div class="content-section">
            <button id="queuePatient" type="submit" class=" btn btn-primary horizontal-button">${ui.message("Assign Patient")}
            </button>
        </div>
    </div>
</form>