<script type="text/javascript">
  jQuery(function () {
    var table = jQuery('#registeredPatientsTable').DataTable({
      searchPanes: true
    });
    table.searchPanes.container().prependTo(table.table().container());
    table.searchPanes.resizePanes();
  });
</script>
<table id="registeredPatientsTable"
       class="table table-striped table-sm table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>#PIN</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Age</th>
        <th>RegisteredBy</th>
        <th>RegisteredOn</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <% todayRegisteredPatientsSummary.each {%>
        <tr>
            <td>${it.identifier}</td>
            <td>${it.name}</td>
            <td>${it.gender}</td>
            <td>${it.age}</td>
            <td>${it.registeredBy}</td>
            <td>${it.registeredDate}</td>
            <td>
                <a href="/botswanaemr/patientProfile.page?patient_id=${it.patientId}"
                   class="color-primary">
                   <i class="fa fa-eye"></i> Profile
                </a>
            </td>
        </tr>
    <% } %>
    </tbody>
</table>