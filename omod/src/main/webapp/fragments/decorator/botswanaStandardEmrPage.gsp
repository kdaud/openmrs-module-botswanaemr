<%
    sessionContext.requireAuthentication()
    def title = config.title ?: ui.message("emr.title")
    def timezoneOffset = -Calendar.getInstance().getTimeZone().getOffset(System.currentTimeMillis()) / 60000
    def jsTimezone = new java.text.SimpleDateFormat("ZZ").format(new Date());

    // Include bootstrap unless specifically excluded
    def includeBootstrap = config.containsKey('includeBootstrap') ? config.includeBootstrap : true;
	ui.includeFragment("appui", "standardEmrIncludes", [ includeBootstrap: includeBootstrap ])
    if (includeBootstrap) {
        ui.includeJavascript("appui", "popper.min.js")
        ui.includeJavascript("appui", "bootstrap.min.js")
        ui.includeCss("appui", "bootstrap.min.css")
    }
    else {
        ui.includeCss("appui", "no-bootstrap.css")
    }
    ui.includeCss("botswanaemr", "font-awesome.min.css")
    ui.includeCss("botswanaemr", "sidebar.css")
    ui.includeCss("botswanaemr", "helper.css")
    ui.includeCss("botswanaemr", "style.css")
    ui.includeCss("botswanaemr", "wizard.css")
    ui.includeCss("botswanaemr", "dataTables.css")
    
    ui.includeJavascript("appui", "jquery-3.4.1.min.js")
    ui.includeJavascript("botswanaemr", "jquery.lineProgressbar.js")
    ui.includeJavascript("botswanaemr", "jquery.validate.min.js")
    ui.includeJavascript("botswanaemr", "jquery.validate-init.js")
    ui.includeJavascript("botswanaemr", "jquery-ui.min.js")
    ui.includeJavascript("botswanaemr", "line-progress-init.js")
    ui.includeJavascript("botswanaemr", "datatables.min.js")
    ui.includeJavascript("botswanaemr", "pace.min.js")
    ui.includeJavascript("botswanaemr", "sidebar.js")
    ui.includeJavascript("botswanaemr", "wizard.js")

    ui.includeCss("referenceapplication", "referenceapplication.css", 100)
    ui.includeJavascript("botswanaemr", "highcharts.js")
    ui.includeJavascript("botswanaemr", "highcharts-grouped-categories.js")
%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>${ title ?: "OpenMRS" }</title>
        <link rel="shortcut icon" type="image/ico" href="/${ ui.contextPath() }/images/openmrs-favicon.ico"/>
        <link rel="icon" type="image/png\" href="/${ ui.contextPath() }/images/openmrs-favicon.png"/>
        <!-- Latest compiled and minified CSS -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${ ui.resourceLinks() }
        <script src="/${ui.contextPath()}/csrfguard" type="text/javascript"></script>
    </head>

    <body>
        <script type="text/javascript">
            var OPENMRS_CONTEXT_PATH = '${ ui.contextPath() }';
            var openmrsContextPath = '/' + OPENMRS_CONTEXT_PATH;
            window.sessionContext = window.sessionContext || {
                locale: "${ ui.escapeJs(sessionContext.locale.toString()) }"
            };
            window.translations = window.translations || {};
            var openmrs = {
                server: {
                    timezone: "${ jsTimezone }",
                    timezoneOffset: ${ timezoneOffset }
                }
            }
        </script>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header navbar-brand">
                    <a href="#">
                        <img class="logo"
                             src="${ui.resourceLink("botswanaemr", "images/code-of-arms.png")}"
                        <span>BotswanaEMR</span>
                    </a>
                </div>
                <ul class="list-unstyled components">
                     <li>
                        <a href="${ui.pageLink("botswanaemr", "registrationAdminDashboard?appId=botswanaemr.registrationAdminDashboard")}">
                            <i class="icon-dashboard"></i> Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="${ui.pageLink("botswanaemr", "registerPatient")}">
                            <i class="fa fa-plus"></i> Patient Registration
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-id-card"></i> Patient Management
                        </a>
                    </li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <!-- /# BotswanaEmrHeader -->
                ${ ui.includeFragment("botswanaemr", "botswanaEMRHeader", [ useBootstrap: includeBootstrap ]) }

                <div class="row">
                    <div class="col float-left">
                        <div class="page-header">
                            <div class="page-title">
                               <ul id="breadcrumbs" class="breadcrumb"></ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col mb-5">
                        ${ ui.includeFragment("botswanaemr", "findPatient")}
                    </div>
                </div>

                <!-- /# Content: loaded differently for each page that uses this decorator -->
                <%= config.content %>
            </div>
        </div>

        <script id="breadcrumb-template" type="text/template">
            <li>
                {{ if (!first) { }}
                <i class="icon-chevron-right link"></i>
                {{ } }}
                {{ if (!last && breadcrumb.link) { }}
                <a href="{{= breadcrumb.link }}">
                {{ } }}
                {{ if (breadcrumb.icon) { }}
                <i class="{{= breadcrumb.icon }} small"></i>
                {{ } }}
                {{ if (breadcrumb.label) { }}
                {{= breadcrumb.label }}
                {{ } }}
                {{ if (!last && breadcrumb.link) { }}
                </a>
                {{ } }}
            </li>
        </script>

        <script type="text/javascript">
            jq(function() {
                emr.updateBreadcrumbs();
            });
            // global error handler
            jq(document).ajaxError(function(event, jqxhr) {
                emr.redirectOnAuthenticationFailure(jqxhr);
            });
            var featureToggles = {};
            <% featureToggles.getToggleMap().each { %>
                featureToggles["${it.key}"] = ${ Boolean.parseBoolean(it.value)};
            <% } %>
        </script>

        <script type="text/javascript">
            jQuery(function () {
                 jQuery("#smartwizard").smartWizard({
                     selected: 0,
                     theme: 'dots',
                     transition: {
                         animation: 'none',
                         speed: '400',
                     },
                     toolbarSettings: {
                         showNextButton: true,
                         showPreviousButton: true,
                         overrideNextButton: true,
                         overridePreviousButton: true,
                         toolbarExtraButtons: [
                         ]
                     },
                     lang: {
                         next: 'Next',
                         previous: 'Previous'
                     },
                     errorSteps: [],
                 });
            });
        </script>
    </body>
</html>