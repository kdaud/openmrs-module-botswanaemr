<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "payment.js")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/index.htm'},
        {
            label: "${ ui.message("registrationapp.registration.label") }",
            link: "${ ui.pageLink("botswanaemr", "regularRegistration") }"
        }
    ]));
</script>

<style>
.zero-margin {
    margin: 0px;
}

.horizontal-margin {
    margin: 0px 1.5rem 0px 0px;
}
</style>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="container-fluid center-content-container content-section">
                <form id="capturePayment" method="POST">
                    <span style=" color: #008000b5;">
                        <i class="fa fa-check-circle  fa-4x"></i>
                    </span>
                    <h5 class="h5 content-section">Registration Completed</h5>

                    <p class="text-center">You have completed the registration of this patient successfully</p>
                    <hr/>

                    <p class="text-center">Complete the payment information around payment before the patient <br/>
                        can proceed to screening and triage or admission
                    </p>
                    <h6 class="h6 content-section">Payment</h6>

                    <p>Please fill out the details around the patients payment</p>


                    <div class="form-group">
                        <label for="amount">Please enter the amount
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="amount"
                               placeholder="Enter the amount BWP">
                    </div>

                    <p>Payment Status</p>

                    <div class="radios">
                        <input id="paymentMade" name="paymentRadio" type="radio"
                               class="horizontal-margin">
                        <label for="paymentMade" class="zero-margin">Payment Made</label>

                        <input name="paymentRadio" id="delayPayment" type="radio" class="horizontal-margin">
                        <label for="delayPayment" class="zero-margin">Delay Payment</label>
                    </div>
                    <div class="content-section">
                        <button id="btnCapturePayment" type="submit"  class=" btn btn-primary horizontal-button" disabled>${ui.message("Next")}
                        </button>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>