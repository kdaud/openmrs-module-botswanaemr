<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/index.htm' },
        { label: "${ ui.message("registrationapp.registration.label") }", link: "${ ui.pageLink("botswanaemr", "registerPatient") }" }
    ]));

    let redirectToNextRegPage = () => {
        if (document.getElementById("emergencyCheck").checked){
            window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/emergencyRegistration.page";
        } else if (document.getElementById("regularCheck").checked){
            window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/regularRegistration.page";
        }

    };
</script>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <form class="simple-form-ui" id="registration" action="javascript:redirectToNextRegPage()">
                    <div class="center-container">
                        <div class="card-title">
                            <h3 class="text-dark">Register a new patient</h3>
                        </div>
                        <h3 class="text-black-50 bold">Select patient type</h3>
                        <p>
                            Select whether the patient will be continue through a regular or emergency registration.
                        </p>
                        <br>
                        <em>
                            What type of registration is this?
                        </em>
                        <br>
                        <div class="radios">
                            <p>
                                <input id="regularCheck" checked="checked" name="radiogroup" type="radio"></input>
                                <label for="regularCheck">Regular</label>
                            </p>
                            <p>
                                <input name="radiogroup" id="emergencyCheck" type="radio"></input>
                                <label for="emergencyCheck">Emergency</label>
                            </p>
                            <br/>
                            <br/>
                            <br>
                            <div>
                                <p style="display: inline">
                                    <button id="next" type="submit"  class=" btn btn-primary right">${ui.message("Next Step")}
                                    </button>

                                </p>
                            </div>
                        </div>
                    </div>

            </form>
        </div>
    </div>
</div>