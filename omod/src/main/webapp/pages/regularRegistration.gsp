<%
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/index.htm'},
        {
            label: "${ ui.message("registrationapp.registration.label") }",
            link: "${ ui.pageLink("botswanaemr", "regularRegistration") }"
        }
    ]));
</script>

<style>
.tab-full {
    height: fit-content !important;
}
</style>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div id="smartwizard" class="px-5">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#step-1">
                            <span class="badge badge-pill badge-primary">1</span>
                            <small class="text-center text-danger">
                                Patient's Details
                            </small>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step-2">
                            <span class="badge badge-pill badge-primary">2</span>
                            <small class="text-center text-danger">
                                Next Of Kin Details
                            </small>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step-3">
                            <span class="badge badge-pill badge-primary">3</span>
                            <small class="text-center text-danger">
                                Confirm All Details
                            </small>
                        </a>
                    </li>
                </ul>

                <div class="container-fluid center-content-container">
                    <div class="tab-content tab-full">
                        <div id="step-1" class="tab-pane content-section text-left" role="tabpanel"
                             aria-labelledby="step-1">
                            <h5 class="h5 content-section">Patient's Details</h5>
                            <h6 class="h6">ID or passport numbers checkup</h6>

                            <p class="text-left content-section">By entering the patients ID or passport number first there will be an<br/>
                                automatic lookup for them through the national database to complete <br/>
                                their generalm details on this form
                            </p>

                            <form id="formPatientDetails">
                                <div class="form-group">
                                    <label for="patientId">ID number
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="form-group is-loading">
                                        <input type="number" class="form-control spinner-text" id="patientId"
                                               name="idNumber" required
                                               placeholder="Enter the patient's ID number" minlength="9">

                                        <div class="spinner-border spinner-section" role="status" id="patientSpinner">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                    <label id="patientIdError"
                                           class="form-text text-danger">ID not found in registry. Register patient manually.</label>
                                </div>

                                <div class="form-group">
                                    <label for="name">Full name
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="name" aria-describedby="nameHelp"
                                           name="fullName"
                                           required placeholder="Enter the patient's full name">
                                    <small id="nameHelp" class="form-text text-muted">Enter first and last name.</small>
                                </div>

                                <div class="form-group">
                                    <label for="gender">Gender
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select class="form-control custom-select" id="gender" name="gender" required>
                                        <option disabled selected>Select the patient's gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="dateOfBirth">Date Of Birth
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="date" class="form-control birth-date" id="dateOfBirth"
                                           name="dateOfBirth"
                                           required placeholder="Patient's date of birth">
                                </div>

                                <div class="form-group">
                                    <label for="age">Age
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" class="form-control" id="age" name="age"
                                           required placeholder="Enter the patient's age">
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="emailAddress"
                                           placeholder="Patient's email address">
                                </div>

                                <div class="form-group">
                                    <label for="phoneNumber">Contact number
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="phoneNumber" name="phoneNumber"
                                           required placeholder="Patient's phone number">
                                </div>

                                <div class="form-group">
                                    <label for="occupation">Occupation
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select type="text" class="form-control custom-select" id="occupation"
                                            name="occupation" required>
                                        <option disabled selected>Select the patient's occupation</option>
                                        <option>Employed</option>
                                    </select>
                                </div>

                                <div class="form-group" id="fgEmployer">
                                    <label for="employer">Name of employer
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="employer" name="employer"
                                           required placeholder="Please enter the name of the employer">
                                </div>

                                <div class="form-group">
                                    <label for="homeAddress">Home address
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="homeAddress" name="homeAddress"
                                           required placeholder="Patient's home address">
                                </div>
                            </form>
                        </div>

                        <div id="step-2" class="tab-pane content-section" role="tabpanel" aria-labelledby="step-2">
                            <h5 class="text-left h5 content-section">Next of Kin Details</h5>
                            <h6 class="text-left h6">ID or passport number check up</h6>

                            <p class="text-left">By entering the patients ID or passport number first there will be an<br/>
                                automatic lookup for them through the national database to <br/>
                                complete their generalm details on this form
                            </p>

                            <form id="formNOK">
                                <div class="form-group">
                                    <label for="nokIDNumber">ID number
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="form-group is-loading">
                                        <input type="number" class="form-control" id="nokIDNumber" name="idNumber"
                                               required
                                               minlength="9" placeholder="Enter next of kin's ID number">
                                        <div class="spinner-border spinner-section" role="status" id="nokSpinner">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                    <label id="nokIdError"
                                           class="form-text text-danger">ID not found in registry. Register patient manually.</label>
                                </div>

                                <div class="form-group">
                                    <label for="nokFullName">Full Name
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="nokFullName" name="fullName" required
                                           placeholder="Enter next of kin's full name">
                                </div>

                                <div class="form-group">
                                    <label for="nokRelationship">Relationship
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select class="custom-select" id="nokRelationship" name="relationship" required>
                                        <option disabled selected>Select the relationship with the patient</option>
                                        <option>Relative</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="nokContact">Contact Number
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="nokContact" name="contactNumber"
                                           required
                                           placeholder="Enter the next of kin's contact number">
                                </div>

                                <div class="form-group">
                                    <label for="nokEmail">Email
                                    </label>
                                    <input type="text" class="form-control" id="nokEmail" name="emailAddress"
                                           placeholder="Enter the next of kin's email">
                                </div>
                            </form>
                        </div>

                        <div id="step-3" class="tab-pane content-section" role="tabpanel" aria-labelledby="step-3">
                            <h5 class="text-left h5 content-section">Confirm details</h5>
                            <h6 class="text-left h6">Please confirm the patient's details below before registering them.</h6>

                            <div class="details-card text-left">
                                <h6 class="h6"><strong>Patient general details</strong></h6>

                                <div id="generalDetails">
                                </div>

                                <h6 class="h6"><strong>Next of kin details</strong></h6>

                                <div id="nokDetails">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="button-section">
                        <button id="btnPrevious" class="btn horizontal-button">Previous</button>
                        <button id="btnNext" class=" btn btn-primary horizontal-button">Complete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
