<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/index.htm' },
        { label: "Registration Admin Dashboard Portal"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="stat-widget-one">
                <div class="stat-icon dib">
                    <i class="fa fa-user-plus color-success border-success"></i>
                </div>
                <div class="stat-content dib">
                    <div class="stat-text">Total Registrations Completed</div>
                    <div class="stat-digit text-success">${allRegisteredPatients}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary") }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Registrations
                            <span class="text-danger">${registeredPatientsDailyAverage}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="stat-widget-one">
                <div class="stat-icon dib">
                    <i class="fa fa-user color-primary border-primary"></i>
                </div>
                <div class="stat-content dib">
                    <div class="stat-text">Today's Registrations</div>
                    <div class="stat-digit text-success">${todayRegisteredPatients}</div>
                        <div class="fullwidth">
                            ${ ui.includeFragment("botswanaemr", "todayGraphSummaries") }
                        </div>
                        <div class="clearfix">
                            <small class="pl-0">
                                Yesterday
                                <span class="text-danger">${yesterdayRegisteredPatients}</span>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                    <div class="card mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-9 pr-0">
                                <h4 class="pl-0"> Today's Registrations</h4>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-3 pr-0">
                                <button
                                        type="submit"
                                        onclick = "redirectToNewRegistrationPage()"
                                        class="btn btn-sm btn-block btn-primary mb-3">
                                        ${ui.message("Register New Patient")}
                                </button>
                                <script type="text/javascript">
                                   let redirectToNewRegistrationPage = () => {
                                       window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
                                   };
                                </script>
                            </div>
                        </div>
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "dailyRegisteredList") }
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                    <div class="card mt-4">
                        <h4>Activity</h4>
                        <% todayRegisteredPatientsList.each { %>
                            <ul class="list-group">
                                <li class="list-group-item">
                                   <small>
                                       <span>${it.creator}</span> created
                                       <span class="text-success">${it.name}</span>
                                       <span class="text-danger">${it.gender}</span>
                                       <span class="text-muted">${it.duration}</span>
                                   </small>
                                </li>
                            </ul>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>