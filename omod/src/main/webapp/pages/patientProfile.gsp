<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
    
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/index.htm' },
        { label: "Patient Profile"}
    ];
</script>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col">
                    <span class="text-primary">
                        <i class="fa fa-user"></i> Logan Paul
                    </span>
                </div>
                <div class="col">
                    <span class="text-success">
                        <i class="fa fa-id-badge"></i> PIN: 5000
                    </span>
                </div>
                <div class="col">
                    <span class="text-danger">
                        <i class="fa fa-id-card"></i> ID: 1234567890
                    </span>
                </div>
            </div>
        </div>
        <div class="container-fluid card shadow d-flex justify-content-center mt-5">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-patient-info-tab"
                        data-toggle="pill" href="#pills-patient-info"
                        role="tab" aria-controls="pills-patient-info"
                        aria-selected="true"> Patient Information
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-medical-info-tab"
                        data-toggle="pill" href="#pills-medical-info"
                        role="tab" aria-controls="pills-medical-info"
                        aria-selected="false"> Basic Medical Information
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-medical-history-tab"
                        data-toggle="pill" href="#pills-medical-history"
                        role="tab" aria-controls="pills-medical-history"
                        aria-selected="false"> Medical History
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-payment-history-tab"
                        data-toggle="pill" href="#pills-payment-history"
                        role="tab" aria-controls="pills-payment-history"
                        aria-selected="false"> Payment History
                    </a>
                </li>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3">
                <!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-patient-info"
                    role="tabpanel" aria-labelledby="pills-patient-info-tab">
                    <div class="row">
                        <div class="col col-sm-10 col-md-10 col-lg-11 pr-0">
                            <h4>Basic Information</h4>
                        </div>
                        <div class="col col-sm-2 col-md-2 col-lg-1 pr-0">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#basicInfoModal">EDIT</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>Name:
                                <span class="text-dark"> Max Payne</span>
                            </span>
                            </br>
                            <span>Contact:
                                <span class="text-dark"> 0712345678</span>
                            </span>
                            <span>Email:
                                <span class="text-dark"> payne@gmail.com</span>
                            </span>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>ID:
                                <span class="text-danger"> 1234567890</span>
                            </span>
                            </br>
                            <span>Age:
                                <span class="text-dark"> 49</span>
                            </span>
                            </br>
                            <span>Gender:
                                <span class="text-dark"> Male</span>
                            </span>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>PIN:
                                <span class="text-success"> 600</span>
                            </span>
                            </br>
                            <span>Occupation: </br>
                                <span class="text-dark"> Coffee Farmer</span>
                            </span>
                            </br>
                            <span>Employer name: </br>
                                <span class="text-dark"> Coffee Marketing Board</span>
                            </span>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>Date Of Birth:
                                <span class="text-success"> 01-01-1980</span>
                            </span>
                            </br>
                            <span>Home address: </br>
                                <span class="text-dark">  22 Thamagana Rd, Broadhurst, Gabarone</span>
                            </span>
                            </br>
                            <span>Registered Date: </br>
                                <span class="text-dark"> 10-07-2020</span>
                            </span>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="basicInfoModal" tabindex="-1" role="dialog" aria-labelledby="basicInfoModalTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header mt-5 pt-5">
                                    <h5 class="modal-title" id="basicInfoModalTitle">Patient's Basic Information</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="patientId">ID number
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" id="patientId" placeholder="Enter patient's ID">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Full name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter full name">
                                        </div>
                                        <div class="form-group">
                                            <label for="gender">Gender
                                                <span class="text-danger">*</span>
                                            </label>
                                            <select class="form-control" id="gender">
                                                <option>Male</option>
                                                <option>Female</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="dateOfBirth">Date Of Birth
                                            <span class="text-danger">*</span>
                                            </label>
                                            <input type="date" class="form-control" id="dateOfBirth" placeholder="Patient's date of birth">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" placeholder="Patient's email address">
                                        </div>
                                        <div class="form-group">
                                            <label for="phoneNumber">Contact number
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="phoneNumber" class="form-control" id="phone" placeholder="Patient's phone number">
                                        </div>
                                        <div class="form-group">
                                            <label for="occupation">Occupation
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="occupation" class="form-control" id="email" placeholder="Patient's occupation">
                                        </div>
                                        <div class="form-group">
                                            <label for="homeAddress">Home address
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="email" class="form-control" id="email" placeholder="Patient's home address">
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        jQuery(function () {
                             jQuery("#basicInfoModal").appendTo("body");
                        });
                    </script>
                    <hr>
                    <div class="row">
                        <div class="col col-sm-10 col-md-10 col-lg-11 pr-0">
                            <h4>Next Of Kin Details</h4>
                        </div>
                        <div class="col col-sm-2 col-md-2 col-lg-1 pr-0">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#nextOfKinModal">EDIT</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>Name:
                                <span class="text-dark"> Max Sr.</span>
                            </span>
                            </br>
                            <span>Email:
                                <span class="text-dark"> srpayne@gmail.com</span>
                            </span>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>ID:
                                <span class="text-danger"> 1234567890</span>
                            </span>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>Relationship:
                                <span class="text-success"> Father</span>
                            </span>
                            </br>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-3">
                            <span>Contact:
                                <span class="text-dark"> 0742345670</span>
                            </span>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="nextOfKinModal" tabindex="-1" role="dialog" aria-labelledby="nextOfKinModalTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="nextOfKinModalTitle">Next Of Kin Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="name">Full name
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter full name">
                                            </div>
                                            <div class="form-group">
                                                <label for="patientId">ID number
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" class="form-control" id="patientId" placeholder="Next of Kin ID">
                                            </div>
                                            <div class="form-group">
                                                <label for="relationship">Relationship
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <select class="form-control" id="relationship">
                                                    <option>Wife</option>
                                                    <option>Father</option>
                                                    <option>Uncle</option>
                                                    <option>Brother</option>
                                                    <option>Sister</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneNumber">Contact number
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="phoneNumber" class="form-control" id="phone" placeholder="Phone number">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email" placeholder="Email address">
                                            </div>
                                            <button class="btn btn-sm btn-block bg-white">
                                                  Add Another next of kin
                                            </button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            jQuery(function () {
                                 jQuery("#nextOfKinModal").appendTo("body");
                            });
                        </script>
                    </div>
                </div>
                <!-- 2nd tab -->
                <div class="tab-pane fade" id="pills-medical-info"
                    role="tabpanel" aria-labelledby="pills-medical-info-tab">
                    <p>2. Medical Info Tab</p>
                </div>
                <!-- 3rd tab -->
                <div class="tab-pane fade" id="pills-medical-history"
                    role="tabpanel" aria-labelledby="pills-medical-history-tab">
                    <p>3. Medical History Tab</p>
                </div>
                <!-- 4th tab -->
                <div class="tab-pane fade" id="pills-payment-history"
                    role="tabpanel" aria-labelledby="pills-payment-history-tab">
                    <p>4. Payment History Tab</p>
                </div>
            </div>
        </div>
    </div>
</div>