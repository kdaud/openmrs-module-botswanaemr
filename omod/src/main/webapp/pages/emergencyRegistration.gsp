<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/index.htm' },
        { label: "${ ui.message("registrationapp.registration.label") }", link: "${ ui.pageLink("botswanaemr", "registerPatient") }" }
    ]));
</script>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>
<div class="row">
  <div class="col">
      <div class="card">
          <div class="center-container">
              <div class="card-title">
                  <h3 class="text-dark">Emergency patient</h3>
              </div>
              <br/>
              <h4 class="text-dark">Patient details</h4>
              <br/>
              <h3 class="text-black-50 body">Emergency patient</h3>
              <p>
                  An emergency patient will have a PIN created for them without any information inputted which can be filled out at a later stage.
              </p>

              <br/>
              <div class="form-group">
                  <label for="emergencyPatientName" class="text-dark">Full name</label>
                  <input type="text" class="form-control" id="emergencyPatientName" placeholder="Enter the patient's full name">
              </div>
              <br/>
              <br/>
              <br>
              <div>
                  <p style="display: inline">
                      <button id="next" type="submit" class=" btn btn-primary right "> ${ui.message("Complete")}
                      </button>

                  </p>
              </div>
          </div>


      </div>
  </div>
</div>