package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaemrUtils;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaemrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.BotswanaemrUtils.formatGender;
import static org.openmrs.module.botswanaemr.BotswanaemrUtils.formatPersonCreator;
import static org.openmrs.module.botswanaemr.BotswanaemrUtils.formatPersonName;

public class RegistrationAdminDashboardPageController {

  public void get(PageModel model, UiSessionContext sessionContext, UiUtils ui) {
    Date today = DateUtil.getStartOfDay(new Date());
    Date yesterDay = DateUtil.adjustDate(today, -1, DurationUnit.DAYS);

    //Check for possibility of null pointer to some results
    int allPatientListSize = 0;
    int todayRegistration = 0;
    int yesterdayPatients = 0;
    List<Patient> allPatients = getAllEverRegisteredPatientsWithLimits(null, null, null);
    if(allPatients != null && allPatients.size() > 0) {
      allPatientListSize = allPatients.size();
    }

    List<Patient> todayPatients = getAllEverRegisteredPatientsWithLimits(today, today, null);
    if(todayPatients != null && todayPatients.size() > 0) {
      todayRegistration = todayPatients.size();
    }

    List<Patient> yesterdayPatientsList = getAllEverRegisteredPatientsWithLimits(yesterDay, yesterDay, null);
    if(yesterdayPatientsList != null && yesterdayPatientsList.size() > 0) {
      yesterdayPatients = yesterdayPatientsList.size();
    }
    model.addAttribute("allRegisteredPatients", allPatientListSize);
    model.addAttribute("todayRegisteredPatients", todayRegistration);
    model.addAttribute("yesterdayRegisteredPatients", yesterdayPatients);
    model.addAttribute("registeredPatientsDailyAverage", getDailyRegisteredAveragePatients());
    model.addAttribute("todayRegisteredPatientsList", patientObjects(getAllEverRegisteredPatientsWithLimits(null, null, 10)));

  }

  private List<Patient> getAllEverRegisteredPatientsWithLimits(Date start, Date end, Integer limit) {
    return Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(start, end, limit);
  }

  private Integer getDailyRegisteredAveragePatients() {

    int totalPatientsEverRegistered = 0;
    Patient firstPatient = null;
    Patient lastPatient = null;
    int daysBetweenDates = 0;

    if(getAllEverRegisteredPatientsWithLimits(null, null, null) != null && getAllEverRegisteredPatientsWithLimits(null, null, null).size() > 0){
      totalPatientsEverRegistered = getAllEverRegisteredPatientsWithLimits(null, null, null).size();
      firstPatient = getAllEverRegisteredPatientsWithLimits(null, null ,null).get(0);
      lastPatient = getAllEverRegisteredPatientsWithLimits(null, null, null).get(getAllEverRegisteredPatientsWithLimits(null, null, null).size() - 1);
    }
    if(firstPatient != null && lastPatient != null) {
      daysBetweenDates = BotswanaemrUtils.unitsSince(lastPatient.getPersonDateCreated(), firstPatient.getPersonDateCreated(), "days");
    }
    if(daysBetweenDates == 0) {
      daysBetweenDates = 1;
    }

    return totalPatientsEverRegistered / daysBetweenDates;

  }

  private List<GeneralPatientObject> patientObjects(List<Patient> patientList) {
    List<GeneralPatientObject> allItems = new ArrayList<GeneralPatientObject>();
    GeneralPatientObject generalPatientObject = null;
    for(Patient patient : patientList) {
      generalPatientObject = new GeneralPatientObject();
      generalPatientObject.setName(formatPersonName(patient.getPersonName()));
      generalPatientObject.setGender(formatGender(patient));
      generalPatientObject.setCreator(formatPersonCreator(patient));
      generalPatientObject.setDuration(formatDurationSinceRegistration(patient));

      allItems.add(generalPatientObject);
    }

    return allItems;
  }

}
