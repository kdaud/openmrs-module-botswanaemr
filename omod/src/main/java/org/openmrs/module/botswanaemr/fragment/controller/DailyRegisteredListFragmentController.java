package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaemrUtils;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.SimplifiedPatient;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DailyRegisteredListFragmentController {
  public void controller(FragmentModel model) {
    Date today = DateUtil.getStartOfDay(new Date());
    model.addAttribute("todayRegisteredPatientsSummary", getSimplifiedPatients(Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(today, today, null)));

  }

  private List<SimplifiedPatient> getSimplifiedPatients(List<Patient> patientList) {
    List<SimplifiedPatient> allPatients = new ArrayList<SimplifiedPatient>();
    SimplifiedPatient simplifiedPatient = null;

    if(patientList != null) {
      for (Patient patient : patientList) {
        simplifiedPatient = new SimplifiedPatient();
        simplifiedPatient.setIdentifier(BotswanaemrUtils.formatPatientIdentifier(patient));
        simplifiedPatient.setPatientId(patient.getPatientId());
        simplifiedPatient.setName(BotswanaemrUtils.formatPersonName(patient.getPersonName()));
        simplifiedPatient.setGender(BotswanaemrUtils.formatGender(patient.getPerson()));
        simplifiedPatient.setAge(patient.getAge().toString());
        simplifiedPatient.setRegisteredBy(BotswanaemrUtils.formatPersonCreator(patient.getPerson()));
        simplifiedPatient.setRegisteredDate(BotswanaemrUtils.formatDateWithTime(patient.getDateCreated()));

        allPatients.add(simplifiedPatient);

      }
    }
    return allPatients;

  }
}
