package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Location;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class PatientQueuePageController {

  public void controller(PageModel model, @RequestParam("location") Location location) {
    model.addAttribute("location", location);
  }
}
