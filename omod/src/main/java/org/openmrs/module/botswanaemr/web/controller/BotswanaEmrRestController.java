package org.openmrs.module.botswanaemr.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientIdentifierType;
import org.openmrs.PersonName;
import org.openmrs.Relationship;
import org.openmrs.api.APIException;
import org.openmrs.api.LocationService;
import org.openmrs.api.PatientService;
import org.openmrs.api.PersonService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.PatientRegistration;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistryService;
import org.openmrs.module.botswanaemr.api.impl.BotswanaPersonRegistryServiceImpl;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.service.IdentifierSourceService;
import org.openmrs.module.registrationcore.RegistrationData;
import org.openmrs.module.registrationcore.api.RegistrationCoreService;
import org.openmrs.module.webservices.rest.web.RestConstants;
import org.openmrs.validator.PatientIdentifierValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.openmrs.module.botswanaemr.BotswanaemrUtils.*;

@Controller
@RequestMapping(value = "/rest/" + RestConstants.VERSION_1 + "/" + BotswanaEmrConstants.BOTSWANAEMR_MODULE_ID)
public class BotswanaEmrRestController {

	private static final String NATIONAL_ID_IDENTIFIER_TYPE = "9C92F3DF-AE32-462F-8FB2-A78DFAF8BA7C";
	private static final String PASSPORT_IDENTIFIER_TYPE = "5E8BFED6-1729-4A9B-AB5A-46B27389EE5F";
	private static final String BIRTH_CERT_IDENTIFIER_TYPE = "3179C66B-3A00-4DEF-AC53-A07B49DF8F54";

	private static final String MALE_GENDER = "M";
	private static final String FEMALE_GENDER = "F";
	private static final String OTHER_GENDER = "O";

	private static IdentifierSource idSource;

	@Autowired
	RegistrationCoreService registrationCoreService;
	
	/** Logger for this class and subclasses */
	protected final Log log = LogFactory.getLog(getClass());

	@Autowired
	private PatientService patientService;

	@Autowired
	private PersonService personService;

	@Autowired
	private LocationService locationService;

	final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper

	@RequestMapping(value = "/registry/{personIdNumber}", method = RequestMethod.GET)
	public @ResponseBody
	BotswanaRegistryResponse SearchPatientInBirthAndDeathRegistry(@PathVariable String personIdNumber) {

		BotswanaPersonRegistryService botswanaPersonRegistryService = new BotswanaPersonRegistryServiceImpl();
		BotswanaRegistryResponse searchResults = botswanaPersonRegistryService.searchPerson(personIdNumber);

		return searchResults;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody
	BotswanaRegistryResponse SavePatientRegistrationDetails(@RequestBody Map<String, String> payload) throws ParseException {
		PatientRegistration patientRegistration = mapper.convertValue(payload, PatientRegistration.class);
		Patient patient = new Patient();

		String fullname = patientRegistration.getFullname();
		PersonName personName = new PersonName();
		personName.setFamilyName(extractFamilyName(fullname));
		personName.setGivenName(extractGivenName(fullname));
		personName.setMiddleName(extractMiddleName(fullname));
		Set<PersonName> personNames = new TreeSet<PersonName>();
		personNames.add(personName);
		patient.setNames(personNames);

		String gender = patientRegistration.getGender();
		if (gender.equalsIgnoreCase("Male")) {
			patient.setGender(MALE_GENDER);
		} else if (gender.equalsIgnoreCase("Female")) {
			patient.setGender(FEMALE_GENDER);
		} else {
			patient.setGender(OTHER_GENDER);
		}

		String dob = patientRegistration.getDob();
		Date birthDate =  (new SimpleDateFormat("yyyy-MM-dd")).parse(dob);
		patient.setBirthdate(birthDate);

		PatientIdentifier patientIdentifier = getDefaultIdentifier(patientRegistration.getIdNumber(), "");
		patient.addIdentifier(patientIdentifier);
		PatientIdentifier openMrsId = validateOrGenerateIdentifier("", null);
		patient.addIdentifier(openMrsId);

		RegistrationData registrationData = new RegistrationData();
		registrationData.setPatient(patient);
		// registrationData.setIdentifier(openMrsId.getIdentifier());

		// registrationData.setRelationships(relationships);
		BotswanaRegistryResponse botswanaRegistryResponse = new BotswanaRegistryResponse();
		try {
			// TODO: Move the Patient Registration logic to a custom class that extends the RegistrationCoreService
			// registrationCoreService.registerPatient(registrationData);
			saveRegistrationDetails(registrationData);
			botswanaRegistryResponse.setResponse("Patient saved successfully");
			botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
	
		} catch (Exception e) {
			botswanaRegistryResponse.setResponse("Unable to register patient: " + e.getMessage());
			botswanaRegistryResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
		}

		return botswanaRegistryResponse;
	}

	public void saveRegistrationDetails(RegistrationData registrationData) {
		Patient patient = registrationData.getPatient();

		if (patient == null) {
			throw new APIException("Patient cannot be null");
		}

		patient = patientService.savePatient(patient);

		List<Relationship> relationships = registrationData.getRelationships();
		ArrayList<String> relationshipUuids = new ArrayList<String>();
		for (Relationship relationship : relationships) {
			if (relationship.getPersonA() == null) {
				relationship.setPersonA(patient);
			} else if (relationship.getPersonB() == null) {
				relationship.setPersonB(patient);
			} else {
				throw new APIException("Only one side of a relationship should be specified");
			}
			personService.saveRelationship(relationship);
			relationshipUuids.add(relationship.getUuid());
		}
	}

	private PatientIdentifier getDefaultIdentifier(String identifierString, String idType) {
// TODO: Handle different Id Types i.e. birth cert, National Id, Passport
		PatientIdentifierType nationalIdNumberIdentifierType = Context.getPatientService()
				.getPatientIdentifierTypeByUuid(NATIONAL_ID_IDENTIFIER_TYPE);
		PatientIdentifier pId = new PatientIdentifier();
		pId.setIdentifier(identifierString);
		pId.setIdentifierType(nationalIdNumberIdentifierType);
		pId.setPreferred(true);

		return pId;
	}

	private PatientIdentifier validateOrGenerateIdentifier(String identifierString, Location identifierLocation) {
		IdentifierSourceService iss = getIssAndUpdateIdSource();
		identifierLocation = getIdentifierLocation(identifierLocation);

		// generate identifier if necessary, otherwise validate
		if (StringUtils.isBlank(identifierString)) {
			identifierString = iss.generateIdentifier(idSource, null);
		} else {
			PatientIdentifierValidator.validateIdentifier(identifierString, idSource.getIdentifierType());
		}

		PatientIdentifier pId = new PatientIdentifier(identifierString, idSource.getIdentifierType(), identifierLocation);
		pId.setPreferred(false);

		return pId;
	}

	private IdentifierSourceService getIssAndUpdateIdSource() {
		IdentifierSourceService iss = Context.getService(IdentifierSourceService.class);
		if (idSource == null) {
			String sourceIdentifier = "1";
			if (StringUtils.isNumeric(sourceIdentifier)) {
				idSource = iss.getIdentifierSource(Integer.valueOf(sourceIdentifier));
			} else {
				idSource = iss.getIdentifierSourceByUuid(sourceIdentifier);
			}
			if (idSource == null) {
				throw new APIException("cannot find identifier source with id:" + sourceIdentifier);
			}
		}
		return iss;
	}

	private Location getIdentifierLocation(Location identifierLocation) {
		if (identifierLocation == null) {
			identifierLocation = locationService.getDefaultLocation();
			if (identifierLocation == null) {
				throw new APIException("Failed to resolve location to associate to patient identifiers");
			}
		}

		return identifierLocation;
	}
}
