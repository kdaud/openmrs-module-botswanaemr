package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Date;

public class QueuePatientFragmentController {
    public void controller(@SpringBean FragmentModel pageModel, @RequestParam("patientId") Patient patient) {
        pageModel.addAttribute("patientId", patient.getPatientId());
        pageModel.addAttribute("locationList", Context.getLocationService().getAllLocations());

    }

    public String post(@RequestParam(value = "patientId") Patient patient, UiUtils uiUtils,
                         @RequestParam(value = "queueRoom") Location location,
                         @RequestParam(value = "queueRoom", required = false) Location queueRoom,
                         UiSessionContext sessionContext) throws IOException {
        PatientQueue patientQueue = new PatientQueue();
        PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
        patientQueue.setLocationFrom(sessionContext.getSessionLocation());
        patientQueue.setPatient(patient);
        patientQueue.setLocationTo(location);
        patientQueue.setStatus(PatientQueue.Status.PENDING);
        patientQueue.setCreator(sessionContext.getCurrentUser());
        patientQueue.setDateCreated(new Date());
        patientQueueingService.assignVisitNumberForToday(patientQueue);

        if (queueRoom != null) {
            patientQueue.setQueueRoom(queueRoom);
        }

       PatientQueue savedQueue = patientQueueingService.savePatientQue(patientQueue);
        System.out.println("CObject to saved is :"+savedQueue);

        //simpleObject.put("toastMessage", new ObjectMapper().writeValueAsString(toastSuccess));


        return "redirect:" + uiUtils.pageLink("botswanaemr", "registerPatient");
    }
}

