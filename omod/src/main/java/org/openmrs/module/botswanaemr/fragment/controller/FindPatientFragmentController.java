package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appframework.service.AppFrameworkService;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class FindPatientFragmentController {

	public void get(PageModel model, @RequestParam(value="appId", required = false) String app) {
		AppFrameworkService appFrameworkService = Context.getService(AppFrameworkService.class);
		AppDescriptor appDescriptor = appFrameworkService.getApp("referenceapplication.registrationapp.registerPatient");
		if (appDescriptor.getConfig().get("registrationAppLink") == null) {
			model.addAttribute("registrationAppLink", "");
		} else {
			model.addAttribute("registrationAppLink", appDescriptor.getConfig().get("registrationAppLink").getTextValue());
		}
		model.addAttribute("columnConfig", appDescriptor.getConfig().get("columnConfig"));
	}
}