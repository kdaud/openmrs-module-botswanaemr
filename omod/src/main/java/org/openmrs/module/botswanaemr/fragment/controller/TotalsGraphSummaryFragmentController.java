package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TotalsGraphSummaryFragmentController {
  public void controller(FragmentModel model) throws ParseException {
    String MON = getDate().get(0);
    String TUE = getDate().get(1);
    String WED = getDate().get(2);
    String THUR = getDate().get(3);
    String FRI = getDate().get(4);
    String SAT = getDate().get(5);
    String SUN = getDate().get(6);

    model.addAttribute("sun", getPatientsSeenOnDay(SUN));
    model.addAttribute("mon", getPatientsSeenOnDay(MON));
    model.addAttribute("tue", getPatientsSeenOnDay(TUE));
    model.addAttribute("wed", getPatientsSeenOnDay(WED));
    model.addAttribute("thur", getPatientsSeenOnDay(THUR));
    model.addAttribute("fri", getPatientsSeenOnDay(FRI));
    model.addAttribute("sat", getPatientsSeenOnDay(SAT));

    //System.out.println("The day of week >>"+getDate());
  }

private Integer getPatientsSeenOnDay(String dateString) throws ParseException {
    int value = 0;
  List<Patient> patientList = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(getDateFromString(dateString), getDateFromString(dateString), null);
    if(patientList != null) {
      value = patientList.size();
    }
    return value;
}

private List<String> getDate() {
  Calendar now = Calendar.getInstance();
  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  List<String> days = new ArrayList<String>();
  int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
  now.add(Calendar.DAY_OF_MONTH, delta );
  for (int i = 0; i < 7; i++)
  {
    days.add(format.format(now.getTime()));
    now.add(Calendar.DAY_OF_MONTH, 1);
  }
  return days;
}
private Date getDateFromString(String dateString) throws ParseException {
  return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
}
}
