package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.Date;
import java.util.List;

public class TodayGraphSummariesFragmentController {
  public void controller(FragmentModel model) {

    model.addAttribute("t00", getPatientsSeenPerHourGiven(new Date(), "00"));
    model.addAttribute("t01", getPatientsSeenPerHourGiven(new Date(), "01"));
    model.addAttribute("t02", getPatientsSeenPerHourGiven(new Date(), "02"));
    model.addAttribute("t03", getPatientsSeenPerHourGiven(new Date(), "03"));
    model.addAttribute("t04", getPatientsSeenPerHourGiven(new Date(), "04"));
    model.addAttribute("t05", getPatientsSeenPerHourGiven(new Date(), "05"));
    model.addAttribute("t06", getPatientsSeenPerHourGiven(new Date(), "06"));
    model.addAttribute("t07", getPatientsSeenPerHourGiven(new Date(), "07"));
    model.addAttribute("t08", getPatientsSeenPerHourGiven(new Date(), "08"));
    model.addAttribute("t09", getPatientsSeenPerHourGiven(new Date(), "09"));
    model.addAttribute("t10", getPatientsSeenPerHourGiven(new Date(), "10"));
    model.addAttribute("t11", getPatientsSeenPerHourGiven(new Date(), "11"));
    model.addAttribute("t12", getPatientsSeenPerHourGiven(new Date(), "12"));
    model.addAttribute("t13", getPatientsSeenPerHourGiven(new Date(), "13"));
    model.addAttribute("t14", getPatientsSeenPerHourGiven(new Date(), "14"));
    model.addAttribute("t15", getPatientsSeenPerHourGiven(new Date(), "15"));
    model.addAttribute("t16", getPatientsSeenPerHourGiven(new Date(), "16"));
    model.addAttribute("t17", getPatientsSeenPerHourGiven(new Date(), "17"));
    model.addAttribute("t18", getPatientsSeenPerHourGiven(new Date(), "18"));
    model.addAttribute("t19", getPatientsSeenPerHourGiven(new Date(), "19"));
    model.addAttribute("t20", getPatientsSeenPerHourGiven(new Date(), "20"));
    model.addAttribute("t21", getPatientsSeenPerHourGiven(new Date(), "21"));
    model.addAttribute("t22", getPatientsSeenPerHourGiven(new Date(), "22"));
    model.addAttribute("t23", getPatientsSeenPerHourGiven(new Date(), "23"));

  }

  private Integer getPatientsSeenPerHourGiven(Date date, String hour) {
    int value = 0;
    List<Patient>  patientList = Context.getService(BotswanaEmrService.class).getPatientsSeenOnDatePerHour(date, hour);
    if(patientList != null) {
      value = patientList.size();
    }

    return value;
  }
}
