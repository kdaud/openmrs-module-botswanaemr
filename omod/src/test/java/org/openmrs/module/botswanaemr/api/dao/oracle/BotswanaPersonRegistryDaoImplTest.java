package org.openmrs.module.botswanaemr.api.dao.oracle;

import junit.framework.TestCase;
import oracle.jdbc.OracleConnection;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryPerson;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BotswanaPersonRegistryDaoImplTest extends TestCase {

	public void setUp() throws Exception {
		super.setUp();
	}

	public void testGetPersonsById() throws SQLException, ParseException {
			BotswanaPersonRegistryDaoImpl botswanaPersonRegistryDao = new BotswanaPersonRegistryDaoImpl();
		List<BotswanaRegistryPerson> personsById = botswanaPersonRegistryDao.getPersonsById("235262727");

		assertThat(personsById.size(), equalTo(1));

	}
}
