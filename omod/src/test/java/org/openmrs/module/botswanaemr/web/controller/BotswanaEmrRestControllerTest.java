package org.openmrs.module.botswanaemr.web.controller;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.openmrs.module.registrationcore.api.RegistrationCoreService;
import org.openmrs.module.registrationcore.api.impl.RegistrationCoreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.UnsupportedEncodingException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class BotswanaEmrRestControllerTest {

	private RegistrationCoreService registrationCoreService;

	private MockMvc mockMvc;

	private BotswanaEmrRestController botswanaEmrRestController;

	private String personIdNumber;

	private String missingPersonIdNumber;

	@Before
	public void startService() {
		MockitoAnnotations.initMocks(this);
		botswanaEmrRestController = new BotswanaEmrRestController();
		mockMvc = MockMvcBuilders.standaloneSetup(botswanaEmrRestController).build();
		personIdNumber = "235262727";
		missingPersonIdNumber = "xxxxxxxxx";
	}

	@Test
	@Ignore
	public void testSearchPatientInBirthAndDeathRegistry_shouldPassIfMatchIsFound() throws Exception {
		MvcResult mvcResult = mockMvc
				.perform(
						get(String.format("/rest/v1/botswanaemr/registry/%s", missingPersonIdNumber)).contentType(
								MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.status").exists())
				.andReturn();

		String content = mvcResult.getResponse().getContentAsString();
		assertThat(content.contains("200"), equalTo(true));
		assertThat(content.contains("235262727"), equalTo(true));
	}

	@Test
	@Ignore
	public void testSearchPatientInBirthAndDeathRegistry_shouldFailIfMatchIsNotFound() throws Exception {
		MvcResult mvcResult = mockMvc
				.perform(
						get(String.format("/rest/v1/botswanaemr/registry/%s", personIdNumber)).contentType(
								MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.status").exists())
				.andReturn();

		String content = mvcResult.getResponse().getContentAsString();

		assertThat(content.contains("404"), equalTo(true));
		assertThat(content.contains("Record not found"), equalTo(true));

	}

	@Test
	@Ignore
	public void testSavePatientRegistrationDetails() throws Exception {
		String requestJson = "{\"idNumber\": \"1234453\",\n    \"fullname\": \"Kenneth O Ochieng\",\n    \"gender\": \"Male\",\n    \"dob\": \"2022-02-01\",\n    \"email\": \"accounts@intellisoftkenya.com\",\n    \"contactNumber\": \"0723862719\",\n    \"occupation\": \"adasd\",\n    \"homeaddress\": \"Kisumu\",\n    \"nokIdNumber\": {},\n    \"nokFullname\": \"adasdasd\",\n    \"nokRelationship\": null,\n    \"nokContact\": \"99276\",\n    \"nokEmail\": \"asdasdasd\"\n}";

		MvcResult mvcResult = mockMvc
				.perform(
						post(String.format("/rest/v1/botswanaemr/registration"))
								.contentType(MediaType.APPLICATION_JSON)
								.content(requestJson))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").exists())
				.andReturn();

		String content = mvcResult.getResponse().getContentAsString();
	}

}
